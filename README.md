This application is part of Valtech front end code challenge. In addition to the `ReactJs` application, a RestAPI is implemented to retrieve data from the server and most of the features were unit tested.

The technologies used are the followings:

- ReactJs 18
- ExpressJs 4.18
- React library testing 13.4
- Jest 29.3

This readme is organized as follows:

1. _Getting started_ section where you have the different steps to follow to setup and run the application
2. _Application screenshots_ section
3. _Automatic testing_ section with a preview of the different tested scenarios

# Getting started

**Server**

1. Go to the `server` folder

```shell
cd server
```

2. Install dependencies

```shell
npm install
```

3. Run server

```shell
npm start
```

4. Run tests with coverage

```shell
npm run test:coverage
```

**Client**

1. Install dependencies

```shell
npm install
```

2. Run application

```shell
npm start
```

3. Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

4. Run tests

```shell
npm run test:watch
```

5. Run tests with coverage

```shell
npm run test:coverage
```

# Application screenshots

- Default display
  ![default display](./images/default-display.png)

- Hover display
  ![hover display](./images/hover-display1.png)

# Automatic testing

- Test the `Country` component rendering by passing a country object as props

  - Test the country's properties are added to the DOM
  - When the user's mouse gets over on the country container, new elements and styles are added to the DOM
  - When the user's mouse gets out, some elements and styles should be retrieved from the DOM

- Test the `App` component by mocking the javaScript’s fetch method
- Test the `/countries` endpoint returns the expected value
