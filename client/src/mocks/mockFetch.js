const countriesListResponse = [
  {
    id: 1,
    name: "Front-end",
    capital: "valtech_",
    imageUrl: `images/valtech_.jpg`,
    description:
      "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Voluptates aspernatur velit alias tempore, molestiae distinctio accusantium!",
  },
  {
    id: 2,
    name: "Italy",
    capital: "venice",
    imageUrl: `images/venice.jpg`,
    description:
      "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Voluptates aspernatur velit alias tempore, molestiae distinctio accusantium!",
  },
];

export const mockFetch = async () => {
  return {
    status: 200,
    json: async () => countriesListResponse,
  };
};
