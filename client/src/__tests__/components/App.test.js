import { render, screen, waitFor } from "@testing-library/react";
import App from "../../App";
import { mockFetch } from "../../mocks/mockFetch";
/**
 * test fetch
 * test length of countries
 *
 * test elements
 * test child elements
 * test image name
 * test name of country to be in document
 * test image to be in document
 *
 * test hover  (user event)
 * test text to appear during hover
 * test element to be removed after hovering
 *
 */

describe("Name of the group", () => {
  beforeEach(() => {
    jest.spyOn(window, "fetch").mockImplementation(mockFetch);
  });

  afterEach(() => {
    jest.restoreAllMocks();
  });

  test("renders name countries", async () => {
    render(<App />);

    await waitFor(() => {
      expect(screen.getByText("Front-end")).toBeInTheDocument();
    });
  });
});
