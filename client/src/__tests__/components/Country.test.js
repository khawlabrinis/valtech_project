import { expect } from "@jest/globals";
import { fireEvent, render, screen } from "@testing-library/react";
import Country from "../../components/country/Country";

beforeEach(() => {
  render(
    <Country
      name="Front-end"
      capital="valtech_"
      imageUrl="images/valtech_.jpg"
      description="khawla"
    />
  );
});

describe("Testing country component rendering", () => {
  it("should render {name, capital, image} of Country when component is mount", () => {
    const backgroundImage = screen.getByTestId("background");
    const countryName = screen.getByText("Front-end");
    const countryCapital = screen.getByRole("heading");

    expect(countryName).toBeInTheDocument();
    expect(countryCapital).toHaveTextContent("valtech_");
    expect(backgroundImage).toHaveStyle(
      `background-image: url(${process.env.REACT_APP_BACKEND_URL}/images/valtech_.jpg)`
    );
    expect(backgroundImage).toBeInTheDocument();
  });

  it("should render the Explore button when user pass mouse over the country container", () => {
    const countryContainer = screen.getByTestId("country");
    const countryDescription = screen.queryByTestId("description");

    fireEvent.mouseOver(countryContainer);
    const exploreButton = screen.getByRole("button");
    expect(exploreButton).not.toBeNull();
    expect(exploreButton).toHaveTextContent("Explore More");
    expect(countryDescription).toBeNull();
  });

  it("should not render the Explore button when user pass mouse out the country container", () => {
    const exploreButton = screen.queryByRole("button");
    const countryContainer = screen.getByTestId("country");

    fireEvent.mouseOver(countryContainer);
    fireEvent.mouseOut(countryContainer);
    expect(exploreButton).toBeNull();
    expect(exploreButton).not.toBeInTheDocument();
  });
});
