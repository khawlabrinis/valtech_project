import { useState } from "react";
import "./Country.scss";

const HoverPart = ({ description }) => {
  return (
    <>
      {description && <span data-testid="description">{description}</span>}{" "}
      <button>Explore More</button>
    </>
  );
};

const Country = ({ name, capital, imageUrl, description }) => {
  const [isHovering, setIsHovering] = useState(false);
  const handleMouseOver = () => {
    setIsHovering(true);
  };

  const handleMouseOut = () => {
    setIsHovering(false);
  };
  return (
    <div
      className="country-container"
      onMouseOver={handleMouseOver}
      onMouseOut={handleMouseOut}
      data-testid="country"
    >
      <div
        className="background-image"
        data-testid="background"
        style={{
          backgroundImage: `url(${process.env.REACT_APP_BACKEND_URL}/${imageUrl})`,
        }}
      />
      <div className="country-body-container">
        <p>{name}</p>
        <h2>{capital}</h2>
        {isHovering && <HoverPart description={description} />}
      </div>
    </div>
  );
};

export default Country;
