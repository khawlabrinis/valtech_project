import Country from "../country/Country";
import "./Countries.scss";

const Countries = ({ countries }) => {
  return (
    <div className="countries-container">
      {countries.map((country) => (
        <Country key={country.id} {...country} />
      ))}
    </div>
  );
};

export default Countries;
