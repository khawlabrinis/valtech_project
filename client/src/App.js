import React from "react";
import Countries from "./components/countries/Countries";

const App = () => {
  const [countries, setCountries] = React.useState([]);

  React.useEffect(() => {
    fetch("/countries")
      .then((res) => res.json())
      .then((data) => setCountries(data));
  }, []);

  return (
    <div>
      <Countries countries={countries} />
    </div>
  );
};

export default App;
