const supertest = require("supertest");
const app = require("../index");

describe("Unit Test", () => {
  test("GET /countries", async () => {
    await supertest(app)
      .get("/countries")
      .expect(200)
      .then((response) => {
        // Check type and length
        expect(Array.isArray(response.body)).toBeTruthy();
        expect(response.body.length).toEqual(7);

        // Check data
        expect(response.body[0].id).toBe(1);
        expect(response.body[0].name).toBe("Front-end");
        expect(response.body[0].capital).toBe("valtech_");
      });
  });
});
