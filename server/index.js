const express = require("express");
const app = express();
const PORT = 3001;
const cors = require("cors");
const countries = require("./data/countries.json");

app.get("/countries", (req, res) => {
  try {
    let result = countries.map((country, i) => {
      const id = i + 1;
      return { id, imageUrl: `/images/${country.capital}.jpg`, ...country };
    });
    res.status(200).json(result);
  } catch (err) {
    res.status(500).send(err);
  }
});

app.use(express.static("public"));
app.use(cors());
app.listen(PORT, () => {
  console.log(`Listening on port ${PORT}`);
});

module.exports = app;
